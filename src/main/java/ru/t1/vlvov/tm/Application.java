package ru.t1.vlvov.tm;

import ru.t1.vlvov.tm.api.ICommandRepository;
import ru.t1.vlvov.tm.constant.ArgumentConst;
import ru.t1.vlvov.tm.constant.TerminalConst;
import ru.t1.vlvov.tm.model.Command;
import ru.t1.vlvov.tm.repository.CommandRepository;
import ru.t1.vlvov.tm.util.FormatUtil;

import java.util.Scanner;

public class Application {

    public final static ICommandRepository COMMAND_REPOSITORY = new CommandRepository();

    public static void main(String[] args) {
        if (processArguments(args)) exit();
        processCommands();
    }

    private static boolean processArguments(final String args[]) {
        if (args == null || args.length == 0) return false;
        final String argument = args[0];
        processArgument(argument);
        return true;
    }

    private static void processArgument(final String argument) {
        switch (argument){
            case ArgumentConst.VERSION:
                showVersion();
                break;
            case ArgumentConst.ABOUT:
                showAbout();
                break;
            case ArgumentConst.HELP:
                showHelp();
                break;
            case ArgumentConst.INFO:
                showInfo();
                break;
            case ArgumentConst.ARGUMENTS:
                showArguments();
                break;
            case ArgumentConst.COMMANDS:
                showCommands();
                break;
            default:
                showErrorArgument();
        }
    }

    private static void processCommands() {
        System.out.println("** WELCOME TO TASK-MANAGER**");
        final Scanner scanner = new Scanner(System.in);
        String command;
        while(true) {
            System.out.println("ENTER COMMAND");
            command = scanner.nextLine();
            processCommand(command);
        }
    }

    private static void processCommand(final String command) {
        if (command == null) {
            showErrorCommand();
            return;
        }
        switch (command){
            case TerminalConst.VERSION:
                showVersion();
                break;
            case TerminalConst.ABOUT:
                showAbout();
                break;
            case TerminalConst.HELP:
                showHelp();
                break;
            case TerminalConst.EXIT:
                exit();
                break;
            case TerminalConst.INFO:
                showInfo();
                break;
            case TerminalConst.COMMANDS:
                showCommands();
                break;
            case TerminalConst.ARGUMENTS:
                showArguments();
                break;
            default:
                showErrorCommand();
        }
    }

    private static void showCommands(){
        System.out.println("[COMMANDS]");
        for (final Command command : COMMAND_REPOSITORY.getTerminalCommands()) {
            final String name = command.getName();
            if (name == null || name.isEmpty()) continue;
            System.out.println(name);
        }
    }

    private static void showArguments(){
        System.out.println("[ARGUMENTS]");
        final Command[] commands = COMMAND_REPOSITORY.getTerminalCommands();
        for (final Command command : commands) {
            final String argument = command.getArgument();
            if (argument == null || argument.isEmpty()) continue;
            System.out.println(argument);
        }
    }

    private static void showInfo() {
        System.out.println("[INFO]");
        final int availableProcessors= Runtime.getRuntime().availableProcessors();
        System.out.println("Available processors (cores): " + availableProcessors);
        final long freeMemory = Runtime.getRuntime().freeMemory();
        final String freeMemoryFormat = FormatUtil.formatBytes(freeMemory);
        System.out.println("Free memory: " + freeMemoryFormat);
        final long maxMemory = Runtime.getRuntime().maxMemory();
        final String maxMemoryValue = FormatUtil.formatBytes(maxMemory);
        final boolean maxMemoryCheck = maxMemory == Long.MAX_VALUE;
        final String maxMemoryFormat = maxMemoryCheck ? "no limit" : maxMemoryValue;
        System.out.println("Maximum memory: " + maxMemoryFormat);
        final long totalMemory = Runtime.getRuntime().totalMemory();
        final String totalMemoryFormat = FormatUtil.formatBytes(totalMemory);
        System.out.println("Total memory: " + totalMemoryFormat);
        final long usageMemory = totalMemory - freeMemory;
        final String usageMemoryFormat = FormatUtil.formatBytes(usageMemory);
        System.out.println("Usage memory: " + usageMemoryFormat);
    }

    private static void exit() {
        System.exit(0);
    }

    private static void showVersion() {
        System.out.println("[VERSION]");
        System.out.println("1.7.0");
    }

    private static void showAbout() {
        System.out.println("[ABOUT]");
        System.out.println("Name: Vladimir Lvov");
        System.out.println("E-mail: vlvov@t1.ru");
    }

    private static void showHelp() {
        System.out.println("[HELP]");
        final Command[] commands = COMMAND_REPOSITORY.getTerminalCommands();
        for (final Command command : commands) System.out.println(command);
    }

    private static void showErrorArgument() {
        System.err.println("Error! This argument not supported");
    }

    private static void showErrorCommand() {
        System.err.println("Error! This command not supported");
    }

}
